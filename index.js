function domId(id) {
  return document.getElementById(id);
}
//Bài 1
function xetTuyen() {
  var diemChuan = domId("diemChuan").value * 1;
  var khuVuc = domId("khuVuc").value * 1;
  var doiTuong = domId("doiTuong").value * 1;
  var diemMon1 = domId("diemMon1").value * 1;
  var diemMon2 = domId("diemMon2").value * 1;
  var diemMon3 = domId("diemMon3").value * 1;
  var divThongBao = domId("ketQuaBai1");

  var diemXetTuyen = tinhDiem(diemMon1, diemMon2, diemMon3, khuVuc, doiTuong);
  if (diemMon1 <= 0 || diemMon2 <= 0 || diemMon3 <= 0) {
    divThongBao.innerHTML = `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`;
  } else {
    if (diemXetTuyen < diemChuan) {
      divThongBao.innerHTML = `Bạn đã rớt. Tổng điểm : ${diemXetTuyen}`;
    } else {
      divThongBao.innerHTML = `Bạn đã đậu. Tổng điểm : ${diemXetTuyen}`;
    }
  }
}

function tinhDiem(diemMon1, diemMon2, diemMon3, khuVuc, doiTuong) {
  var tongDiem = diemMon1 + diemMon2 + diemMon3 + khuVuc + doiTuong;
  return tongDiem;
}

// Bài 2

function tinhTienDien() {
  var hoTen = domId("hoTen").value;
  var soKw = domId("soKw").value * 1;
  tinhTien(hoTen, soKw);
}

function tinhTien(hoTen, soKw) {
  var giaTien50KwDau = 500;
  var giaTien50KwKe = 650;
  var giaTien100KwKe = 850;
  var giaTien150KwKe = 1100;
  var giaTienKwConLai = 1300;

  var tongTien = 0;

  if (soKw <= 50) {
    tongTien = soKw * giaTien50KwDau;
  } else if (soKw <= 100) {
    tongTien = giaTien50KwDau * 50 + (soKw - 50) * giaTien50KwKe;
  } else if (soKw <= 200) {
    tongTien =
      giaTien50KwDau * 50 + giaTien50KwKe * 50 + (soKw - 100) * giaTien100KwKe;
  } else if (soKw <= 350) {
    tongTien =
      giaTien50KwDau * 50 +
      giaTien50KwKe * 50 +
      giaTien100KwKe * 100 +
      (soKw - 200) * giaTien150KwKe;
  } else {
    tongTien =
      giaTien50KwDau * 50 +
      giaTien50KwKe * 50 +
      giaTien100KwKe * 100 +
      giaTien150KwKe * 150 +
      (soKw - 350) * giaTienKwConLai;
  }

  domId("ketQuaBai2").innerHTML = `Họ Tên : ${hoTen}; Tiền Điện : ${tongTien} `;
}
